#!/usr/bin/env bash

mvn clean package

cp target/happystore*.war docker-tomcat

docker build --rm -t happy_local docker-tomcat

export appimage=happy_local

docker network create docker_app_happystoreDevNet

docker tag localhost:5000/happystoretestdb dockerregistry:5000/happystoretestdb

docker-compose -f docker-compose-app.yml -f docker-compose-local.yml up