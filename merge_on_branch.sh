#!/usr/bin/env bash

#Pas top robuste, mais je fais au plus simple !
set -e

# for your information
whoami

if [ -z ${SRC_BRANCHE+x} ]; then
    echo "var is unset"
    exit 1
else
    echo "SRC_BRANCHE is set to '$SRC_BRANCHE'"
fi

if [ -z ${TARGET_BRANCHE+x} ]; then
    echo "var is unset"
    exit 2
else
    echo "TARGET_BRANCHE is set to '$TARGET_BRANCHE'"
fi

# we need to extract the ssh/git URL as the runner uses a tokenized URL
echo "CI_REPOSITORY_URL = $CI_REPOSITORY_URL"
CI_PUSH_REPO=`echo $CI_REPOSITORY_URL | sed -E 's#.*@(.*?)/(.*?)/#git@\1:\2/#g'`
echo "Repo : $CI_PUSH_REPO"

ps -ef | grep ssh-agent

ping -c 1 gitlab

git config --global user.name "GitLab Docker Runner"
git config --global user.email "kerny@octo.com"

git remote add originssh $CI_PUSH_REPO

git fetch originssh $TARGET_BRANCHE:$TARGET_BRANCHE
git log --graph --oneline --decorate master uat

oldtree=$(git log -n 1 --pretty=%T HEAD)
echo "Old Tree = $oldtree"
newcommit=$(echo "Merge branch '${SRC_BRANCHE}' to '${TARGET_BRANCHE}'" | git commit-tree ${oldtree} -p ${SRC_BRANCHE} -p ${TARGET_BRANCHE})

echo "refs/heads/${TARGET_BRANCHE}"
echo "$SRC_BRANCHE $TARGET_BRANCHE"
echo $newcommit

git update-ref "refs/heads/${TARGET_BRANCHE}" "$newcommit"

git remote set-url --push originssh "${CI_PUSH_REPO}"

# push changes
# always return true so that the build does not fail if there are no changes
git push originssh ${TARGET_BRANCHE}:${TARGET_BRANCHE} || true
