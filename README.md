# Prérequis
Il faut avoir suivi la procédure du repo _GitLabCIInfra_ : https://gitlab.com/gitlabcidemo/git-lab-ci-infra.

## Accès au repo Git du serveur gitlab depuis votre poste
Le plus simple est de se faire une entrée dans /etc/hosts
```
127.0.0.1    gitlabdocker
```

Ensuite vous pouvez vous connecter sur votre repo distant avec :
```
git remote add origin git@gitlabdocker:root/git-lab-ci-demo.git
# Et eventuellement
git branch --set-upstream-to=origin/master master
```

## Conf réseau pour dockerrunner
Pour une raison mysterieuse autour dockerrunner qui n'utilise pas sa conf réseau
lorsqu'il lance des commandes docker, il faut aussi avoir cette entrée
```
# Gitlab eval
127.0.0.1         dockerregistry
```
On peut reproduire le pb avec la commande suivante qui ne passe que avec l'entrée dans /etc/host
```
docker run -it --network docker_gitlabNet --privileged -v /var/run/docker.sock:/var/run/docker.sock  dockercompose:17-dind docker login -u dockerci -p GhpbGlwcGUiLCJuYmYiOjE1MTExMzIyMTcsImV4cCI6 dockerregistry:5000
```

# Comment utiliser ce repo
1. Cloner ce repo en local
1. Aller sur le repo pour desactiver la protection de la branch (setting du projet), ce qui
permettra de pousser 'sauvagement' n'importe quel commit sur le repository distant.
1. Ajouter un remote repository vers votre projet Git qui tourne sur l'infra (celui que vous venez de créer).
```
git remote add origin git@gitlabdocker:root/git-lab-ci-demo.git
```
4. Créer une banche master :
```
git co -b master
```
5. Positionner la sur le commit qui vous intéresse :
```
# Exemple Step 1
git reset --hard (git log --oneline demo | grep "Step 1" | cut -d" " -f1)

# Exemple Step 2a
git reset --hard (git log --oneline demo | grep "Step 2a" | cut -d" " -f1)
```
6. Pousser le commit sur le repo distant :
```
git push --force origin master
```
7. Rependre au 5. pour un nouveau commit.

# Début de la démo

## Step 0 : juste un projet Maven
On peut lancer un build.
Rien dans les pipelines sur le serveur.
http://localhost:480/root/git-lab-ci-demo/pipelines

## Step 1 : notre premier pipeline
On fait un build avec maven en jdk7.
=> On définit des phases et des jobs
=> On utilise un cache pour accélérer les builds
=> On choisit l'image du build
=> On enregistre un artifact
=> On peut le voir après le build

## Step 2 : le projet sur la même phase se lance en parallèle
Step 2 bis : on peut factoriser des bouts de code YML


## Step 3 : On build avec docker
=> Avec l'artifact précédent on build 2 versions de docker : jdk7 et 8
=> Les images sont poussées dans le repo (avec les clées indiquées)

## Step 4 : On déploie master mais aussi les features branches
=> Commentaire de ce qui n'est pas nécessaire
=> On parle de l'extention docker-compose on top of docker:17-dind
=> Variabilisation du docker compose
=> de l'image par défaut pour le build
=> focus sur only branch
=> Déclaration de l'environnement => création de l'environnement dynamique
=> et du destroy

## Step 5 : Fork sur FF1
=> On deploy sur FF1 br avec environnement dynamique
=> L'environnement est provisionné

## Step 6 : On fait une merge Request sur master
=> On voit le pipeline
=> L'environnement est déprovisionné
=> La modif est propagée sur master

## Step 7 : on pousse en UAT et production
=> on fait une branche UAT et une production ON LA POUSSE SUR LE REMOTEgit checkout $TARGET_BRANCHE
=> Qu'est-ce qui change ? On ne va pas rebuilder, on veut juste promouvoir les binaires
==> tags
==> deploiement
=> on exclut le build classique de ces branches, elle devrait être protégée dans l'absolue


## Step 8 : Sonar et tout actif
=> On va visiter sonar, c'est vide
=> On le lance et ensuite on va le voir, on voit a

## Step 8 : Création d'une Merge request pour aller en production
==> Todo,
Voir http://ermaker.github.io/blog/ko/2016/08/05/create-merge-request-automatically-on-gitlab.html

