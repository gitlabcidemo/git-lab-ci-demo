drop sequence IF EXISTS  hibernate_sequence;
drop table IF EXISTS SaleOperation;
drop table IF EXISTS SaleTransaction;
drop table IF EXISTS Stock;
drop table IF EXISTS Store;
drop table IF EXISTS Product;
drop table IF EXISTS CategoryFamily_VAT;
drop table IF EXISTS CategoryFamily;
drop table IF EXISTS VAT;
drop table IF EXISTS Country;

