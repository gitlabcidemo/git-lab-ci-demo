package com.octo.red.happystore.controller;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.Timed;
import com.octo.red.happystore.model.TurnoverVo;
import com.octo.red.happystore.services.TurnoverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

@Controller
public class TurnoverController {

	private static final Logger logger = LoggerFactory.getLogger(TurnoverController.class);
	
	@Autowired
	private TurnoverService turnoverService;

    @Resource(name="getMetricRegistry")
    private MetricRegistry metrics;

    private Timer turnover;

    @PostConstruct
    private void init(){
        turnover = metrics.timer(MetricRegistry.name(TurnoverController.class, "getTurnover"));
    }

    @RequestMapping(value = "/turnover", method = RequestMethod.GET)
    public @ResponseBody List<TurnoverVo> getTurnover(@RequestParam("groupId") int groupId) {
        final Timer.Context context = turnover.time();
        try {

            logger.info("Request received [groupId={}] on /turnover", groupId);

            return turnoverService.computeTurnover(groupId);
        } finally {
            context.stop();
        }
    }

}
