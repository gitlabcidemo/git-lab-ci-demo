package com.octo.red.happystore.controller;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Locale;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Resource(name="getMetricRegistry")
    private MetricRegistry metrics;

    private Timer home;

    @PostConstruct
    private void init(){
        home = metrics.timer(MetricRegistry.name(HomeController.class, "home"));
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
        final Timer.Context context = home.time();
        try {
            logger.info("Request received on /");
            return "home";
        } finally {
            context.stop();
        }
	}
}
