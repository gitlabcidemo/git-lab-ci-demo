package com.octo.red.happystore.controller;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.Timed;
import com.octo.red.happystore.model.InventoryRecord;
import com.octo.red.happystore.services.InventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.List;

@Controller
public class InventoryController {
	
	private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);
	
	@Autowired
	private InventoryService inventoryService;

    @Resource(name="getMetricRegistry")
    private MetricRegistry metrics;

    private Timer responses;

    @PostConstruct
    private void init(){
        responses = metrics.timer(MetricRegistry.name(InventoryController.class, "getInventory"));
    }

    @RequestMapping(value = "/inventory", method = RequestMethod.GET)
    public @ResponseBody List<InventoryRecord> getInventory(@RequestParam("storeId") long storeId) {
        final Timer.Context context = responses.time();
        try {
            //List(produit, nombre, catégorie, sous-total par catégorie)
            logger.info("Request received [store={}] on /inventory", storeId);
            return inventoryService.list(storeId);
        } finally {
            context.stop();
        }
    }

}
